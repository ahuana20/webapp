# Ruta de aprendizaje

Desarrollo de web de productos en Django 3.0

SISTEMAS WEB DE VENTAS (beta).

Actividades (estimadas) programadas 
Semana 1: 
– Programación Funcional/ decoradores/GIT
– Introducción a POO en Python
– Creación de un paquete Python y publicación en PyPI
– Compilación e instalación de paquetes con dependencias complejas
Django Framework:
– Introducción a Django
– Patrón de arquitectura MVC y MTV
– Creación de estructura de proyectos en la línea de comandos
–  Gestión de URLs, vistas y plantillas
– Creación de Vistas
– Vistas basadas en clases, en funciones y configuración de URLs.
– Plantillas, bloques, herencia, etiquetas y filtros
Semana 2
Modelo de datos
– Modelos y API para base de datos
– Tipos de campos
– Creación de consultas 
Gestión de formularios
– Creación de Formularios
– Plantilla de un formulario
– Validación y sanitación de datos usando formularios y validadores
Django Admin
– Introducción a Django admin y personalización básica
– Personalización y registro de modelos
– Creación de campos calculados y formularios personalizados
Semana 3
Middleware y Sesiones
– Manejos de sesiones
– Autenticación y autorización
Diseño de APIs RESTful
– Introducción a REST, APIs de Hipermedios 
– Serialización de modelos y otras fuentes de datos
– Ruteadores, conjuntos de vistas, AJAX 
Reportes y XLS
– xlutils
Semana 4  
Proyecto personal
- Creación de primer Proyecto con Django
- Configurar una base de datos (por defecto SQLite)
- Configurar base de datos PostgreSQL
- Crear primer modelo en django, crear migraciones y a poder ver los resultados de nuestra tabla creada (entidad).
- Modelos: Aprender sobre las relaciones ForeignKey y ManyToManyField en los modelos.
- ORM de Django y hacer las consultas básicas de un CRUD y más consultas que se pueden hacer con el mismo.
- Panel administrador Django.
- URLs en Django y Configuración en primeras Views.
- Templates en Django y como configurarlos, aprenderemos a enviar datos en nuestro template.
- Templates en Django, como enviar objectos a nuestra plantilla, utilizar condicionales, y aplicar herencia entre plantillas y la creación de bloques de código en las herencias.
- Archivos estáticos en Django y como configurarlos en el settings.
- Utilizar Plantilla AdminLTE3 con Django, usando configuración de los archivos estáticos.
- Crear primer listado con la primera entidad del proyecto, así mismo crear plantilla base de listado.



Semana 5  
- Aprender sobre la vista basada en clase ListView.
- Implementar la librería de DataTables en listado, crear nuevos bloques del template base list.
- Método dispatch y como sobrescribirlo en las vistas basadas en clases.
- Implementar un decorador en el método dispatch en las vistas basadas en clases.
- Sobreescribir el método post en las vistas basadas en clases, implementar el decorador csrf_exempt para desactivar el CSRF.
- Implementar AJAX contra el método post de nuestra vista basadas en clases.
- Vista basadas en clase CreateView, Crearemos nuestro primer formulario basado en una clase.
Semana 6  
- Personalizar formulario, usar la librería django-widget-tweaks.
- CreateView - Validaciones en nuestro formulario.
- Implementar Ajax para procesar los datos del formulario y recibirlo en CreateView.
- Utilizar la vista genérica UpdateView para poder editar el formulario, Ajax.
- Usar vista genérica DeleteView para poder eliminar la información del formulario del listado.
Semana 7  
- Implementar Ajax con nuestra vista DeleteView para poder eliminar un registros.
- Cargar  listado mediante ajax y datatable, ademas de personalizar algunos campos.
- Utilizar la vista genérica TemplateView y crear index .
- Utilizar la vista genérica FormView y crear propios errores sobrescribiendo el método clean.
- Utilizar la vista genérica LoginView e Inicio de sesión y  FormView para realizar login en aplicación web.
- Usar las vistas genéricas LogoutView y RedirectView  para poder cerrar la sesión.
- Validando nuestras vistas (login_required, next, LOGIN_URL).
Semana 8 
- Implementando alertas en nuestro formulario con jquery-confirm.
- Archivos media files, configurar variables MEDIA_ROOT y MEDIA_URL y usar AJAX para envio de datos.
- Select anidados con Ajax/ select2 en django.
- Personalizar  modelo user de Django utilizando la clase AbstractUser y aumentar un atributo.
- Auditoria en modelos con django-crum.

- Crear/Editar/Eliminar un registro de un cliente pero utilizaremos los modal de Boostrap.
- Implementar el autocomplete para la búsqueda de datos, utilizaremos la librería de jquery-ui/ librería de select2.
- Crear clase Mixin , definir métodos y LoginRequiredMixin para validar nuestra vista.
- Usar mixin para validar permisos de usuario – PermissionRequiredMixin.
Semana 9
- Maquetar el diseño y adaptacion de plugins en factura, para el proceso de venta productos.
- Agregando items al detalle de la venta.
- Calcular totales de factura a medida que se añadan productos.
- Programar el evento cantidad/eliminar del detalle de factura.
- Guardar los datos de factura
- Consultar el detalle de venta Modal/Child rows datatable
- Programar editar factura/ Agregar items al detalle de venta(Select2) 
